package br.com.al_pa.sistemaDeCampeonatos.model;

public class GroupScoreboardPrinter {
	
	private Group group;

	public GroupScoreboardPrinter(Group group) {
		this.group = group;
	}
	
	public void printScoreboard() {
		System.out.println("Nome - Pontos - Saldo de Rounds");
		for (Team team : group.getTeams()) {
			TeamScore teamScore = group.getScoreOf(team);
			System.out.println(team.getName() + " --- " + teamScore.getPoints() + " --- " + teamScore.getRoundDifference());
		}
	}

}
