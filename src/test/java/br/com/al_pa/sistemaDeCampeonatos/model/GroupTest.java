package br.com.al_pa.sistemaDeCampeonatos.model;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class GroupTest {

	private List<Team> teams;
	private Group group;
	private static final int TOTAL_NUMBER_OF_TEAMS = 5;

	@Before
	public void setup() {
		int numberOfTeams = TOTAL_NUMBER_OF_TEAMS;
		this.teams = new ArrayList<Team>();
		for (int i = 0; i < numberOfTeams; i++) {
			Team team = Mockito.mock(Team.class);
			this.teams.add(team);
		}
		this.group = new Group(this.teams);
	}

	@Test
	public void shouldBeConstructedWith5Teams() {
		List<Team> teams = new ArrayList<>();
		int numberOfTeams = TOTAL_NUMBER_OF_TEAMS;
		for (int i = 0; i < numberOfTeams; i++) {
			Team team = Mockito.mock(Team.class);
			teams.add(team);
		}
		new Group(teams);
	}

	@Test(expected = RuntimeException.class)
	public void shouldThrowExceptionIfConstructedWithMoreThan80Teams() {
		List<Team> teams = new ArrayList<>();
		int wrongNumberOfTeams = 6;
		for (int i = 0; i < wrongNumberOfTeams; i++) {
			Team team = Mockito.mock(Team.class);
			teams.add(team);
		}
		new Group(teams);
	}

	@Test(expected = RuntimeException.class)
	public void shouldThrowExceptionIfConstructedWithLessThan80Teams() {
		List<Team> teams = new ArrayList<>();
		int wrongNumberOfTeams = 4;
		for (int i = 0; i < wrongNumberOfTeams; i++) {
			Team team = Mockito.mock(Team.class);
			teams.add(team);
		}
		new Group(teams);
	}

	@Test
	public void shouldHaveTheCorrectNumberOfMatches() {
		int correctNumberOfMatches = numberOfDifferentMatchesBetweenNTeams(TOTAL_NUMBER_OF_TEAMS);
		Assert.assertEquals(correctNumberOfMatches, group.getMatches().size());

	}

	private int numberOfDifferentMatchesBetweenNTeams(int totalNumberOfTeams) {
		int totalNumber = totalNumberOfTeams * (totalNumberOfTeams - 1) / 2;
		return totalNumber;
	}

}
