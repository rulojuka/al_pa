package br.com.al_pa.sistemaDeCampeonatos.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TeamTest {

	@Test
	public void shouldBeConstructedWithAName() {
		String teamName = "Team name";
		Team team = new Team(teamName);

		assertEquals(teamName, team.getName());
	}

}
