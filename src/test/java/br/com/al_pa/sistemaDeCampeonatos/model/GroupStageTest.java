package br.com.al_pa.sistemaDeCampeonatos.model;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class GroupStageTest {

	private static final int TOTAL_NUMBER_OF_TEAMS = 80;
	private static final int NUMBER_OF_TEAMS_IN_A_GROUP = 5;

	private List<Team> teams = new ArrayList<>();
	private GroupStage groupStage;

	@Before
	public void setup() {
		int numberOfTeams = TOTAL_NUMBER_OF_TEAMS;
		for (int i = 0; i < numberOfTeams; i++) {
			Team team = Mockito.mock(Team.class);
			teams.add(team);
		}
		groupStage = new GroupStage(teams);
	}

	@Test
	public void shouldBeConstructedWith80Teams() {
		List<Team> teams = new ArrayList<>();
		int numberOfTeams = 80;
		for (int i = 0; i < numberOfTeams; i++) {
			Team team = Mockito.mock(Team.class);
			teams.add(team);
		}
		new GroupStage(teams);
	}

	@Test(expected = RuntimeException.class)
	public void shouldThrowExceptionIfConstructedWithMoreThan80Teams() {
		List<Team> teams = new ArrayList<>();
		int wrongNumberOfTeams = 81;
		for (int i = 0; i < wrongNumberOfTeams; i++) {
			Team team = Mockito.mock(Team.class);
			teams.add(team);
		}
		new GroupStage(teams);
	}

	@Test(expected = RuntimeException.class)
	public void shouldThrowExceptionIfConstructedWithLessThan80Teams() {
		List<Team> teams = new ArrayList<>();
		int wrongNumberOfTeams = 79;
		for (int i = 0; i < wrongNumberOfTeams; i++) {
			Team team = Mockito.mock(Team.class);
			teams.add(team);
		}
		new GroupStage(teams);
	}

	@Test
	public void shouldSplitTeamsInGroups() {
		int rightNumberOfGroups = TOTAL_NUMBER_OF_TEAMS / NUMBER_OF_TEAMS_IN_A_GROUP;
		int numberOfGroups = groupStage.getGroups().size();
		Assert.assertEquals(rightNumberOfGroups, numberOfGroups);
	}

}
