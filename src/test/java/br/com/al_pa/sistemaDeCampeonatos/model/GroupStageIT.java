package br.com.al_pa.sistemaDeCampeonatos.model;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GroupStageIT {

	private static final int TOTAL_NUMBER_OF_TEAMS = 80;
	private GroupStage groupStage;

	@Before
	public void setup() {
		List<Team> teams = new ArrayList<>();
		for (int i = 0; i < TOTAL_NUMBER_OF_TEAMS; i++) {
			String teamName = "Team " + i;
			Team team = new Team(teamName);
			teams.add(team);
		}
		groupStage = new GroupStage(teams);
	}

	@Test
	public void shouldBeFinishedOnlyWhenAllGroupsAreFinished() {
		for (Group group : groupStage.getGroups()) {
			Assert.assertFalse(groupStage.isFinished());
			for (GroupStageMatch match : group.getMatches()) {
				Team aTeamOfTheMatch = match.getTeams().get(0);
				while (!match.isFinished()) {
					match.addRoundToTeam(aTeamOfTheMatch);
				}
			}
			Assert.assertTrue(group.isFinished());
		}
		Assert.assertTrue(groupStage.isFinished());
	}

}
