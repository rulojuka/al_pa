package br.com.al_pa.sistemaDeCampeonatos.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GroupIT {

	private Group group;
	private static final int TOTAL_NUMBER_OF_TEAMS = 5;
	private static final int NUMBER_OF_CLASSIFIED_TEAMS = 2;

	@Before
	public void setup() {
		List<Team> teams = new ArrayList<Team>();
		for (int i = 0; i < TOTAL_NUMBER_OF_TEAMS; i++) {
			String teamName = "Team " + i;
			Team team = new Team(teamName);
			teams.add(team);
		}
		this.group = new Group(teams);
	}

	@Test
	public void shouldBeFinishedOnlyWhenAllMatchesAreFinished() {
		for (GroupStageMatch match : group.getMatches()) {
			Assert.assertFalse(group.isFinished());
			Team teamOfTheMatch = match.getTeams().get(0);
			while (!match.isFinished()) {
				match.addRoundToTeam(teamOfTheMatch);
			}
			Assert.assertTrue(match.isFinished());
		}
		Assert.assertTrue(group.isFinished());
	}

	@Test
	public void shouldGetTheBestTwoClassifiedTeamsAfterGroupIsFinished() {
		
		
		for (GroupStageMatch match : group.getMatches()) {
			Team teamOfTheMatch = match.getTeams().get(0);
			while (!match.isFinished()) {
				match.addRoundToTeam(teamOfTheMatch);
			}
		}
		Assert.assertTrue(group.isFinished());
		Collection<Team> classifiedTeams = group.getClassifiedTeams();
		GroupScoreboardPrinter groupScoreboardPrinter = new GroupScoreboardPrinter(group);
		groupScoreboardPrinter.printScoreboard();
		
		
		
		int numberOfTeams = classifiedTeams.size();
		Assert.assertEquals(NUMBER_OF_CLASSIFIED_TEAMS, numberOfTeams);
	}

}
