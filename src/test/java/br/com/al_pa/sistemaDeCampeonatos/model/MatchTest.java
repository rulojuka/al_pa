package br.com.al_pa.sistemaDeCampeonatos.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class MatchTest {

	private Team teamOne;
	private Team teamTwo;
	private GroupStageMatch match;

	private static final int WINNING_NUMBER_OF_ROUNDS = 16;

	@Before
	public void setup() {
		teamOne = Mockito.mock(Team.class);
		teamTwo = Mockito.mock(Team.class);
		List<Team> teams = Arrays.asList(teamOne, teamTwo);
		match = new GroupStageMatch(teams);
	}

	@Test
	public void shouldBeConstructedWithTwoTeams() {
		List<Team> teams = Arrays.asList(teamOne, teamTwo);
		GroupStageMatch newMatch = new GroupStageMatch(teams);

		assertNotNull(newMatch);
	}

	@Test
	public void shouldBeInitializedWithZeroPointsToEachTeam() {
		List<Team> teams = Arrays.asList(teamOne, teamTwo);
		GroupStageMatch newMatch = new GroupStageMatch(teams);

		assertEquals(0, newMatch.getRoundsOf(teamOne));
		assertEquals(0, newMatch.getRoundsOf(teamTwo));
	}

	@Test
	public void shouldBePossibleToAddRoundsAndGetRoundsOfATeam() {
		match.addRoundToTeam(teamOne);
		match.addRoundToTeam(teamOne);
		match.addRoundToTeam(teamTwo);

		assertEquals(2, match.getRoundsOf(teamOne));
		assertEquals(1, match.getRoundsOf(teamTwo));
	}

	@Test
	public void shouldNotBeFinishedUnlessTheWinningNumberOfRounds() {
		match.addRoundToTeam(teamOne);
		match.addRoundToTeam(teamOne);

		assertFalse(match.isFinished());

		for (int i = 0; i < WINNING_NUMBER_OF_ROUNDS; i++) {
			match.addRoundToTeam(teamTwo);
		}

		assertTrue(match.isFinished());

	}

	@Test(expected = RuntimeException.class)
	public void shouldThrowExceptionWhenTryingToAddPointsInAFinishedMatch() {
		for (int i = 0; i < WINNING_NUMBER_OF_ROUNDS; i++) {
			match.addRoundToTeam(teamTwo);
		}
		assertTrue(match.isFinished());
		match.addRoundToTeam(teamTwo);
	}

	@Test
	public void shouldGetPointsOfATeamInAMatch() {
		for (int i = 0; i < WINNING_NUMBER_OF_ROUNDS; i++) {
			match.addRoundToTeam(teamTwo);
		}
		assertTrue(match.isFinished());
		assertEquals(0,match.pointsWonBy(teamOne));
		assertEquals(1,match.pointsWonBy(teamTwo));
	}

	@Test
	public void shouldGetPointDifference() {
		int teamOnePoints = 5;
		int teamTwoPoints = 8;
		for (int i = 0; i < teamOnePoints; i++) {
			match.addRoundToTeam(teamOne);
		}
		for (int i = 0; i < teamTwoPoints; i++) {
			match.addRoundToTeam(teamTwo);
		}
		assertEquals(teamOnePoints - teamTwoPoints, match.getRoundDifferenceFor(teamOne));
		assertEquals(teamTwoPoints - teamOnePoints, match.getRoundDifferenceFor(teamTwo));
	}
	
	@Test
	public void shouldGetTeams() {
		List<Team> teams = match.getTeams();
		boolean containsTeamOne = teams.contains(teamOne);
		boolean containsTeamTwo = teams.contains(teamTwo);
		Assert.assertTrue(containsTeamOne);
		Assert.assertTrue(containsTeamTwo);
	}

}
