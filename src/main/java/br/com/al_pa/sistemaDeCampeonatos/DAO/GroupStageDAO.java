package br.com.al_pa.sistemaDeCampeonatos.DAO;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.al_pa.sistemaDeCampeonatos.model.GroupStage;

@Repository
public class GroupStageDAO {

	@PersistenceContext
	private EntityManager manager;

	public void save(GroupStage groupStage) {
		manager.persist(groupStage);
	}

	public GroupStage findOne(Integer id) {
		return manager.find(GroupStage.class, id);
	}

}
