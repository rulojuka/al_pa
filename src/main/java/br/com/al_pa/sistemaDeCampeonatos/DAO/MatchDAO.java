package br.com.al_pa.sistemaDeCampeonatos.DAO;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.al_pa.sistemaDeCampeonatos.model.Match;

@Repository
public class MatchDAO {

	@PersistenceContext
	private EntityManager manager;

	public Match findOne(Integer id) {
		return manager.find(Match.class, id);
	}

	public void save(Match match) {
		manager.persist(match);
	}

}
