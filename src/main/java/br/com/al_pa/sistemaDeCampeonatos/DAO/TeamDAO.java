package br.com.al_pa.sistemaDeCampeonatos.DAO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.al_pa.sistemaDeCampeonatos.model.Team;

@Repository
public class TeamDAO {

    @PersistenceContext
    private EntityManager manager;


    public Team findOne(Integer id) {
        return manager.find(Team.class, id);
    }

    public void save(Team team) {
        manager.persist(team);
    }

    public List<Team> findAll() {
        return manager.createQuery("select t from Team t", Team.class).getResultList();
    }

    public void delete(Integer id) {
        manager.remove(findOne(id));
    }
}