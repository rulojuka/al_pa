package br.com.al_pa.sistemaDeCampeonatos.model;

public class TeamScore implements Comparable<TeamScore>{
	
	private int points;
	private int roundDifference;
	private Team team;
	
	public TeamScore(Team team) {
		this.points = 0;
		this.roundDifference = 0;
		this.team = team;
	}
	
	public void addMatch(GroupStageMatch match) {
		if(!match.isFinished()) {
			throw new RuntimeException();
		}
		points += match.pointsWonBy(this.team);
		roundDifference += match.getRoundDifferenceFor(team);
	}

	@Override
	public int compareTo(TeamScore other) {
		if(this.points != other.points) {
			return other.points - this.points;
		}else if(this.roundDifference != other.roundDifference){
			return other.roundDifference - this.roundDifference;
		}else {
			return 0; // Both teams are tied. A tiebreak rule could be applied.
		}
	}

	public int getPoints() {
		return points;
	}

	public int getRoundDifference() {
		return roundDifference;
	}

}
