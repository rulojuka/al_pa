package br.com.al_pa.sistemaDeCampeonatos.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE, isGetterVisibility = Visibility.NONE)
@PrimaryKeyJoinColumn(name = "id")
public class GroupStageMatch extends Match {

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "group_id")
	private Group group;

	public GroupStageMatch(List<Team> teams) {
		super(teams);
	}

	/**
	 * @deprecated Hibernate only
	 */
	public GroupStageMatch() {
		super();
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

}
