package br.com.al_pa.sistemaDeCampeonatos.model;

import java.util.Arrays;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.al_pa.sistemaDeCampeonatos.exception.MatchAlreadyFinishedException;
import br.com.al_pa.sistemaDeCampeonatos.exception.MatchBetweenSameTeamException;
import br.com.al_pa.sistemaDeCampeonatos.exception.MatchNotFinishedYetException;
import br.com.al_pa.sistemaDeCampeonatos.exception.MatchWithMoreOrLessThanTwoTeamsException;
import br.com.al_pa.sistemaDeCampeonatos.exception.TeamNotInTheMatchException;

@Entity
@Table(name = "`Match`") // Match is a SQL reserved word
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Match {

	private static final int NUMBER_OF_TEAMS_IN_A_MATCH = 2;
	private static final int WINNING_NUMBER_OF_ROUNDS = 16;

	@Id
	@GeneratedValue()
	private Integer id;

	@ManyToOne
	private Team teamOne;
	@ManyToOne
	private Team teamTwo;

	private int teamOneRounds;
	private int teamTwoRounds;

	public Match(List<Team> teams) {
		if (teams.size() != NUMBER_OF_TEAMS_IN_A_MATCH) {
			throw new MatchWithMoreOrLessThanTwoTeamsException();
		} else if (teams.get(0).equals(teams.get(1))) {
			throw new MatchBetweenSameTeamException();
		} else {
			this.teamOne = teams.get(0);
			this.teamTwo = teams.get(1);
			this.teamOneRounds = 0;
			this.teamTwoRounds = 0;
		}
	}

	public void addRoundToTeam(Team team) {
		if (this.isFinished()) {
			throw new MatchAlreadyFinishedException();
		}
		if (this.teamOne.equals(team)) {
			teamOneRounds++;
		} else if (this.teamTwo.equals(team)) {
			teamTwoRounds++;
		} else {
			throw new TeamNotInTheMatchException();
		}
	}

	public boolean isFinished() {
		return (teamOneRounds == WINNING_NUMBER_OF_ROUNDS || teamTwoRounds == WINNING_NUMBER_OF_ROUNDS);
	}

	private boolean wasWonBy(Team team) {
		if (this.isFinished()) {
			return this.getWinner().equals(team);
		} else {
			throw new MatchNotFinishedYetException();
		}
	}

	public Team getWinner() {
		if (teamOneRounds == WINNING_NUMBER_OF_ROUNDS)
			return teamOne;
		if (teamTwoRounds == WINNING_NUMBER_OF_ROUNDS)
			return teamTwo;
		throw new MatchNotFinishedYetException();
	}

	public int getRoundDifferenceFor(Team team) {
		if (teamOne.equals(team)) {
			return teamOneRounds - teamTwoRounds;
		} else if (teamTwo.equals(team)) {
			return teamTwoRounds - teamOneRounds;
		} else {
			throw new TeamNotInTheMatchException();
		}
	}

	public int getRoundsOf(Team team) {
		if (teamOne.equals(team)) {
			return teamOneRounds;
		} else if (teamTwo.equals(team)) {
			return teamTwoRounds;
		} else {
			throw new TeamNotInTheMatchException();
		}
	}

	public int pointsWonBy(Team team) {
		if (this.wasWonBy(team)) {
			return 1;
		} else {
			return 0;
		}
	}

	public String getScore() {
		String score = "";
		score += "(" + teamOne.getName() + ")";
		score += " ";
		score += teamOneRounds;
		score += " vs ";
		score += teamTwoRounds;
		score += " ";
		score += "(" + teamTwo.getName() + ")";
		return score;
	}

	public List<Team> getTeams() {
		return Arrays.asList(teamOne, teamTwo);
	}

	/**
	 * @deprecated Hibernate only
	 */
	public Match() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Team getTeamOne() {
		return teamOne;
	}

	public void setTeamOne(Team teamOne) {
		this.teamOne = teamOne;
	}

	public Team getTeamTwo() {
		return teamTwo;
	}

	public void setTeamTwo(Team teamTwo) {
		this.teamTwo = teamTwo;
	}

	public int getTeamOneRounds() {
		return teamOneRounds;
	}

	public void setTeamOneRounds(int teamOneRounds) {
		this.teamOneRounds = teamOneRounds;
	}

	public int getTeamTwoRounds() {
		return teamTwoRounds;
	}

	public void setTeamTwoRounds(int teamTwoRounds) {
		this.teamTwoRounds = teamTwoRounds;
	}

}
