package br.com.al_pa.sistemaDeCampeonatos.model;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "`Group`") // Group is a SQL reserved word
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE, isGetterVisibility = Visibility.NONE)
@PrimaryKeyJoinColumn(name = "id")
public class Group extends Phase {

	private static final int TOTAL_NUMBER_OF_TEAMS = 5;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "groupStage_id")
	private GroupStage groupStage;

	public Group(Collection<Team> teams) {
		super(teams);
		if (this.teams.size() != TOTAL_NUMBER_OF_TEAMS) {
			throw new RuntimeException();
		}
	}

	@Override
	protected void createAllMatches() {
		this.matches = new HashSet<GroupStageMatch>();
		for (int i = 0; i < TOTAL_NUMBER_OF_TEAMS; i++) {
			for (int j = i + 1; j < TOTAL_NUMBER_OF_TEAMS; j++) {
				GroupStageMatch match = new GroupStageMatch(Arrays.asList(teams.get(i), teams.get(j)));
				match.setGroup(this);
				matches.add(match);
			}
		}
	}

	@Override
	protected List<Team> getFirstClassifiedTeams() {
		return Arrays.asList(teams.get(0), teams.get(1));
	}

	public GroupStage getGroupStage() {
		return groupStage;
	}

	public void setGroupStage(GroupStage groupStage) {
		this.groupStage = groupStage;
	}

	/**
	 * @deprecated Hibernate only
	 */
	public Group() {
	}

}
