package br.com.al_pa.sistemaDeCampeonatos.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Phase {

	@Id
	@GeneratedValue
	private Integer id;

	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "group")
	protected List<Team> teams;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "group", fetch = FetchType.EAGER)
	protected Set<GroupStageMatch> matches;

	@JsonIgnore
	@Transient
	private Map<Team, TeamScore> teamScores;
	@JsonIgnore
	@Transient
	private boolean isScoreCalculated = false;
	@JsonIgnore
	@Transient
	private Comparator<Team> compareByTeamScore = (Team o1, Team o2) -> this.teamScores.get(o1)
			.compareTo(this.teamScores.get(o2));

	public Phase(Collection<Team> teams) {
		this.teams = new ArrayList<Team>(teams);
		createAllMatches();
	}

	protected abstract void createAllMatches();

	private void initializeTeamScores() {
		teamScores = new HashMap<Team, TeamScore>();
		for (Team team : teams) {
			TeamScore teamScore = new TeamScore(team);
			teamScores.put(team, teamScore);
		}
	}

	public boolean isFinished() {
		return matches.stream().allMatch(match -> match.isFinished());
	}

	public Collection<GroupStageMatch> getMatches() {
		return this.matches;
	}

	public Collection<Team> getTeams() {
		return this.teams;
	}

	public Collection<Team> getClassifiedTeams() {
		if (!isFinished()) {
			throw new RuntimeException();
		}

		if (!isScoreCalculated) {
			initializeTeamScores();
			calculateScores();
		}

		Collections.sort(teams, compareByTeamScore);
		List<Team> classifiedTeams = getFirstClassifiedTeams();
		return classifiedTeams;
	}

	protected abstract List<Team> getFirstClassifiedTeams();

	private void calculateScores() {
		for (GroupStageMatch match : matches) {
			for (Team team : match.getTeams()) {
				teamScores.get(team).addMatch(match);
			}
		}
		isScoreCalculated = true;
	}

	public TeamScore getScoreOf(Team team) {
		return this.teamScores.get(team);
	}

	/**
	 * @deprecated Hibernate only
	 */
	public Phase() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setTeams(List<Team> teams) {
		this.teams = teams;
	}

	public void setMatches(Set<GroupStageMatch> matches) {
		this.matches = matches;
	}

}
