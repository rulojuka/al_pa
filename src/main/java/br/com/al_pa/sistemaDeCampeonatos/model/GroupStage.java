package br.com.al_pa.sistemaDeCampeonatos.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@Entity
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE, isGetterVisibility = Visibility.NONE)
public class GroupStage {

	private static final int TOTAL_NUMBER_OF_TEAMS = 80;
	private static final int NUMBER_OF_TEAMS_IN_A_GROUP = 5;

	@Id
	@GeneratedValue
	private Integer id;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "groupStage")
	private List<Group> groups;

	public GroupStage(Collection<Team> teams) {
		if (teams.size() != TOTAL_NUMBER_OF_TEAMS) {
			throw new RuntimeException();
		}
		this.groups = splitTeamsInRandomGroups(teams);
	}

	private List<Group> splitTeamsInRandomGroups(Collection<Team> teams) {
		int numberOfTeams = teams.size();
		List<Group> splitGroups = new ArrayList<>();
		if (numberOfTeams % 5 != 0) {
			throw new RuntimeException();
		}
		List<Team> shuffledTeams = new ArrayList<>(teams);
		Collections.shuffle(shuffledTeams);

		Iterator<Team> iterator = shuffledTeams.iterator();
		while (iterator.hasNext()) {
			List<Team> teamsOfAGroup = new ArrayList<>();
			for (int i = 0; i < NUMBER_OF_TEAMS_IN_A_GROUP; i++) {
				teamsOfAGroup.add(iterator.next());
			}
			splitGroups.add(new Group(teamsOfAGroup));
		}
		return splitGroups;
	}

	public List<Group> getGroups() {
		return groups;
	}

	public Set<GroupStageMatch> getMatches() {
		Set<GroupStageMatch> matches = new HashSet<>();
		for (Group group : groups) {
			Collection<GroupStageMatch> matchesOfAGroup = group.getMatches();
			for (GroupStageMatch match : matchesOfAGroup) {
				matches.add(match);
			}
		}
		return matches;
	}

	public boolean isFinished() {
		return groups.stream().allMatch(group -> group.isFinished());
	}

	/**
	 * @deprecated Hibernate only
	 */
	public GroupStage() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

}
