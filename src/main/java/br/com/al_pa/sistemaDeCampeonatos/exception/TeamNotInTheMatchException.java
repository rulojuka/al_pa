package br.com.al_pa.sistemaDeCampeonatos.exception;

@SuppressWarnings("serial")
public class TeamNotInTheMatchException extends IllegalArgumentException {
	
	public TeamNotInTheMatchException() {
		super("This team is not playing this match.");
	}

}
