package br.com.al_pa.sistemaDeCampeonatos.exception;

@SuppressWarnings("serial")
public class MatchBetweenSameTeamException extends IllegalArgumentException {

	public MatchBetweenSameTeamException() {
		super("The teams in a match cannot be the same.");
	}
}
