package br.com.al_pa.sistemaDeCampeonatos.exception;

@SuppressWarnings("serial")
public class MatchWithMoreOrLessThanTwoTeamsException extends IllegalArgumentException {

	public MatchWithMoreOrLessThanTwoTeamsException() {
		super("There should be exactly two teams in a match.");
	}
}
