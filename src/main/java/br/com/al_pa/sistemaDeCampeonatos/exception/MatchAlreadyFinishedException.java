package br.com.al_pa.sistemaDeCampeonatos.exception;

@SuppressWarnings("serial")
public class MatchAlreadyFinishedException extends RuntimeException {
	
	public MatchAlreadyFinishedException() {
		super("This match is already finished.");
	}

}
