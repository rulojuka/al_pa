package br.com.al_pa.sistemaDeCampeonatos.exception;

@SuppressWarnings("serial")
public class MatchNotFinishedYetException extends RuntimeException {

	public MatchNotFinishedYetException() {
		super("This match is not finished yet.");
	}
}
