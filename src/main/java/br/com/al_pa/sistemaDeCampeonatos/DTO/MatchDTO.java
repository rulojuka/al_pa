package br.com.al_pa.sistemaDeCampeonatos.DTO;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.al_pa.sistemaDeCampeonatos.model.GroupStageMatch;
import br.com.al_pa.sistemaDeCampeonatos.model.Team;

public class MatchDTO {

	@JsonProperty("teams")
	private List<Team> teams;

	public List<Team> getTeams() {
		return teams;
	}

	public void setTeams(List<Team> teams) {
		this.teams = teams;
	}

	public GroupStageMatch toMatch() {
		return new GroupStageMatch(teams);
	}

}
