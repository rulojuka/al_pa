package br.com.al_pa.sistemaDeCampeonatos;

import java.util.Arrays;
import java.util.List;

import br.com.al_pa.sistemaDeCampeonatos.model.GroupStageMatch;
import br.com.al_pa.sistemaDeCampeonatos.model.Team;

/**
 * Hello world!
 *
 */
public class TestMatchAndTeams 
{
    public static void main( String[] args )
    {
        Team teamOne = new Team("Team One");
        Team teamTwo = new Team("Team Two");
        List<Team> teams = Arrays.asList(teamOne, teamTwo);
        GroupStageMatch match = new GroupStageMatch(teams);
        
        System.out.println("Foi criada a partida: " + match);
        
        int teamOnePoints = 10;
        int teamTwoPoints = 16;
        
        for(int i=0; i<teamOnePoints; i++) {
        	match.addRoundToTeam(teamOne);
        	System.out.println("O placar é: " + match.getScore());
        }
        for(int i=0; i<teamTwoPoints; i++) {
        	match.addRoundToTeam(teamTwo);
        	System.out.println("O placar é: " + match.getScore());
        }
        System.out.println("O placar é: " + match.getScore());
        if(match.isFinished()) {
        	System.out.println("A partida acabou e o vencedor é " + match.getWinner().getName());
        }
    }
}
