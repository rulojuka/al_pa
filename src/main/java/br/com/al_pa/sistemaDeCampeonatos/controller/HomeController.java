package br.com.al_pa.sistemaDeCampeonatos.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

	@GetMapping("/")
	public String index() {
		return "Bem vindo ao sistema de campeonatos";
	}
}
