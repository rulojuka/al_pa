package br.com.al_pa.sistemaDeCampeonatos.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.al_pa.sistemaDeCampeonatos.DAO.GroupStageDAO;
import br.com.al_pa.sistemaDeCampeonatos.model.GroupStage;
import br.com.al_pa.sistemaDeCampeonatos.model.Team;
import br.com.al_pa.sistemaDeCampeonatos.service.GroupStageService;

@RestController
@RequestMapping("/groupStage/")
public class GroupStageController {

	@Autowired
	GroupStageService service;
	@Autowired
	private GroupStageDAO dao;

	@GetMapping("/{id}")
	@Transactional
	public ResponseEntity<GroupStage> findOne(@PathVariable Integer id) {
		GroupStage groupStage = dao.findOne(id);
		if (groupStage == null) {
			return new ResponseEntity<GroupStage>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<GroupStage>(groupStage, HttpStatus.OK);
		}
	}

	@PostMapping("/random")
	@Transactional
	public ResponseEntity<GroupStage> createRandom() {
		GroupStage groupStage = service.createRandom();
		dao.save(groupStage);
		return new ResponseEntity<GroupStage>(groupStage, HttpStatus.CREATED);
	}

	@PostMapping("/random/populate")
	@Transactional
	public ResponseEntity<GroupStage> createRandomPopulated() {
		GroupStage groupStage = service.createRandom();
		service.populateRandom(groupStage);
		dao.save(groupStage);
		return new ResponseEntity<GroupStage>(groupStage, HttpStatus.CREATED);
	}

	@PutMapping("/{id}/populate")
	@Transactional
	public ResponseEntity<GroupStage> findOneAndPopulate(@PathVariable Integer id) {
		GroupStage groupStage = dao.findOne(id);
		if (groupStage == null) {
			return new ResponseEntity<GroupStage>(HttpStatus.NOT_FOUND);
		}
		if (groupStage.isFinished()) {
			return new ResponseEntity<GroupStage>(HttpStatus.BAD_REQUEST);
		}
		service.populateRandom(groupStage);
		dao.save(groupStage);
		return new ResponseEntity<GroupStage>(groupStage, HttpStatus.OK);
	}
	
	@GetMapping("/{id}/classified")
	@Transactional
	public ResponseEntity<List<Team>> getClassifiedTeams(@PathVariable Integer id) {
		GroupStage groupStage = dao.findOne(id);
		if (groupStage == null) {
			return new ResponseEntity<List<Team>>(HttpStatus.NOT_FOUND);
		}
		if (!groupStage.isFinished()) {
			return new ResponseEntity<List<Team>>(HttpStatus.BAD_REQUEST);
		}
		List<Team> classifiedTeams = service.getClassifiedTeams(groupStage);
		return new ResponseEntity<List<Team>>(classifiedTeams, HttpStatus.OK);
	}

}
