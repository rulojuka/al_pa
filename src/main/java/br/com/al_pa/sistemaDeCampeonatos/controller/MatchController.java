package br.com.al_pa.sistemaDeCampeonatos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.al_pa.sistemaDeCampeonatos.DAO.MatchDAO;
import br.com.al_pa.sistemaDeCampeonatos.DTO.MatchDTO;
import br.com.al_pa.sistemaDeCampeonatos.model.Match;
import br.com.al_pa.sistemaDeCampeonatos.model.Team;

@RestController
@RequestMapping("/match/")
public class MatchController {

	@Autowired
	private MatchDAO dao;

	@PostMapping("/")
	@Transactional
	public ResponseEntity<Match> addOne(@RequestBody MatchDTO matchDTO) {
		Match match = matchDTO.toMatch();
		dao.save(match);
		return new ResponseEntity<Match>(match, HttpStatus.CREATED);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Match> findOne(@PathVariable Integer id) {
		Match match = dao.findOne(id);
		if (match == null) {
			return new ResponseEntity<Match>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Match>(match, HttpStatus.OK);
		}
	}

	// This action can be optimized to run on the database
	@PostMapping("/{id}/addRoundToTeam") // POST because this action is not idempotent
	@Transactional
	public ResponseEntity<Match> addRoundToTeam(@PathVariable Integer id, @RequestBody Team team) {
		Match match = dao.findOne(id);
		if (match == null) {
			return new ResponseEntity<Match>(HttpStatus.NOT_FOUND);
		}
		match.addRoundToTeam(team);
		dao.save(match);
		return new ResponseEntity<Match>(match, HttpStatus.OK);
	}

}
