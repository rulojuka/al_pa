package br.com.al_pa.sistemaDeCampeonatos.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.al_pa.sistemaDeCampeonatos.DAO.TeamDAO;
import br.com.al_pa.sistemaDeCampeonatos.model.Team;

@RestController
@RequestMapping("/team/")
public class TeamController {

	@Autowired
	private TeamDAO dao;

	@GetMapping("/")
	public List<Team> findAll() {
		return dao.findAll();
	}

	@PostMapping("/")
	@Transactional
	public ResponseEntity<Team> addOne(@RequestBody Team team) {
		dao.save(team);

		return new ResponseEntity<Team>(team, HttpStatus.CREATED);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Team> findOne(@PathVariable Integer id) {
		Team team = dao.findOne(id);
		if (team == null) {
			return new ResponseEntity<Team>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Team>(team, HttpStatus.OK);
		}
	}

	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<Team> delete(@PathVariable Integer id) {
		Team team = dao.findOne(id);
		if (team == null) {
			return new ResponseEntity<Team>(HttpStatus.NOT_FOUND);
		} else {
			dao.delete(id);
			return new ResponseEntity<Team>(HttpStatus.NO_CONTENT);
		}
	}

}
