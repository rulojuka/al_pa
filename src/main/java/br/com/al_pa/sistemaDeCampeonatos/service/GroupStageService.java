package br.com.al_pa.sistemaDeCampeonatos.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import br.com.al_pa.sistemaDeCampeonatos.model.Group;
import br.com.al_pa.sistemaDeCampeonatos.model.GroupStage;
import br.com.al_pa.sistemaDeCampeonatos.model.GroupStageMatch;
import br.com.al_pa.sistemaDeCampeonatos.model.Team;

@Service
public class GroupStageService {

	public GroupStage createRandom() {
		List<Team> teams = new ArrayList<>();
		for (int i = 0; i < 80; i++) {
			String teamName = "Team " + i;
			teams.add(new Team(teamName));
		}

		GroupStage groupStage = new GroupStage(teams);
		List<Group> groups = groupStage.getGroups();

		for (Group group : groups) {
			group.setGroupStage(groupStage);
			Collection<GroupStageMatch> matchesOfThisGroup = group.getMatches();
			for (GroupStageMatch match : matchesOfThisGroup) {
				match.setGroup(group);
			}
			Collection<Team> teamsOfThisGroup = group.getTeams();
			for (Team team : teamsOfThisGroup) {
				team.setGroup(group);
			}
		}

		return groupStage;
	}

	public void populateRandom(GroupStage groupStage) {
		Set<GroupStageMatch> matches = groupStage.getMatches();
		for (GroupStageMatch match : matches) {
			List<Team> teams = match.getTeams();
			while (!match.isFinished()) {
				if (Math.random() > 0.5) {
					match.addRoundToTeam(teams.get(0));
				} else {
					match.addRoundToTeam(teams.get(1));
				}

			}
		}
	}

	public List<Team> getClassifiedTeams(GroupStage groupStage) {
		ArrayList<Team> classifiedTeams = new ArrayList<Team>();
		for (Group group : groupStage.getGroups()) {
			classifiedTeams.addAll(group.getClassifiedTeams());
		}
		return classifiedTeams;
	}

}
