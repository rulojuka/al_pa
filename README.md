# Sistema de campeonatos

O serviço está disponível no [heroku](https://sistema-de-campeonatos.herokuapp.com)

###### O conteúdo disponível no Heroku é o da branch [deployHeroku](https://bitbucket.org/rulojuka/al_pa/src/deployHeroku/) que não contém a preparação do modelo (herança e modelo do bd) para a adição dos playoffs

## Processo geral da resolução do desafio

O processo da resolução do desafio está disponível [nesse link](docs/processo.MD)

## Desenho da arquitetura

O projeto foi construído como um monolito, por razões descritas no [processo](docs/processo.MD). A modelagem das entidades e sua persistência no banco foi a seguinte (creio que mostrar a descrição das tabelas seja mais eficiente que um UML nesse caso :) )

```
mysql> desc `GroupStage`;
+-------+---------+------+-----+---------+-------+
| Field | Type    | Null | Key | Default | Extra |
+-------+---------+------+-----+---------+-------+
| id    | int(11) | NO   | PRI | NULL    |       |
+-------+---------+------+-----+---------+-------+
mysql> desc `Group`;
+---------------+---------+------+-----+---------+-------+
| Field         | Type    | Null | Key | Default | Extra |
+---------------+---------+------+-----+---------+-------+
| id            | int(11) | NO   | PRI | NULL    |       |
| groupStage_id | int(11) | YES  | MUL | NULL    |       |
+---------------+---------+------+-----+---------+-------+
mysql> desc `Match`;
+---------------+---------+------+-----+---------+-------+
| Field         | Type    | Null | Key | Default | Extra |
+---------------+---------+------+-----+---------+-------+
| id            | int(11) | NO   | PRI | NULL    |       |
| teamOneRounds | int(11) | NO   |     | NULL    |       |
| teamTwoRounds | int(11) | NO   |     | NULL    |       |
| group_id      | int(11) | YES  | MUL | NULL    |       |
| teamOne_id    | int(11) | YES  | MUL | NULL    |       |
| teamTwo_id    | int(11) | YES  | MUL | NULL    |       |
+---------------+---------+------+-----+---------+-------+
mysql> desc `Team`;
+----------+--------------+------+-----+---------+-------+
| Field    | Type         | Null | Key | Default | Extra |
+----------+--------------+------+-----+---------+-------+
| id       | int(11)      | NO   | PRI | NULL    |       |
| name     | varchar(255) | NO   |     | NULL    |       |
| group_id | int(11)      | YES  | MUL | NULL    |       |
+----------+--------------+------+-----+---------+-------+
```


## Descrição dos endpoints/actions (Modelagem do serviço)

A lista completa de endpoints e actions está descrita [aqui](docs/endpoints-e-actions.MD)

## Testes

Os testes automatizados estão disponíveis em `src/test/` e podem ser executados através do comando `mvn test`, além disso, um arquivo com as requisições prontas para serem importadas no Postman está disponível em `postman/requisicoes_export.json`.

## Instruções para deploy (desenvolvimento e produção)

O projeto utiliza:

* Java 8
* Maven
* MySQL5

#### Desenvolvimento

Preencha o arquivo `src/main/resources/application.properties` com as informações do seu banco de dados e execute o seguinte comando na pasta raiz:
```
mvn jetty:run
```
Isso vai subir a aplicação na porta padrão do Jetty (8080).

#### Produção

A única configuração diferente em produção são as informações do banco de dados, portanto o `Procfile` copia as informações de `src/main/resources/application-prod.properties` para `src/main/resources/application.properties` e sobe o arquivo compactado `.war` utilizando o plugin `webapp-runner` no Heroku.

P.S.: As informações do banco de produção estão em plaintext no `application-prod.properties` caso seja necessário dar uma olhada no banco. Em um projeto real, essa configuração seria acessada pela variável de ambiente já configurada no Heroku.

## Instruções para verificar a aplicação

Dentre todas as ações disponíveis na API, as mais fáceis de utilizar para verificar a aplicação são:

`POST /groupStage/random` para criar uma fase de grupos completa. A requisição pode demorar em torno de 10 segundos, mas como cada criação dessa será feita muito raramente, isso não é um gargalo do sistema. Como essa requisição não coloca rounds nos confrontos, pode-se testar essa funcionalidade central da aplicação com a requisição `POST /match/{id}/addRoundToTeam`

`POST /groupStage/random/populate` para criar uma fase de grupos já completa com as partidas terminadas. Assim, é possível pegar o resultado final com a requisição `GET /groupStage/{id}/classified` que retorna os times classificados quando a fase de grupos já está terminada.

## Conclusão

Creio que o resultado final é condizente com o tempo de um pouco mais de "meia semana de trabalho" usando uma semana normal de trabalho no escritório e também de apenas um desenvolvedor, trabalhando em um projeto do zero. Além disso, muitas coisas ficaram planejadas para um futuro próximo, como a quebra nos dois serviços "TIMES" e "CONFRONTOS" e nos serviços periféricos que surgirão, melhoria do processo de build para uma entrega contínua e aumento da cobertura de testes, dentre outras.

Por fim, ficarei contente em conversar sobre o desafio, explicando quaisquer decisões técnicas (escolha da stack, monolito vs quebrar em serviços, etc), de processo (redução do escopo, pesquisa e conversa com colegas, etc) e outras que possam surgir! Gostei muito do desafio e de tudo que ele fez eu refletir, praticar e construir. Espero que eu possa construir soluções para problemas semelhantes junto com vocês! Obrigado!